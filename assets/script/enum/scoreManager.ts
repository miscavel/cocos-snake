export enum SCORE_MANAGER_EVENT {
  SCORE_UPDATE = 'score_update',
  HIGHSCORE_UPDATE = 'highscore_update',
}