
import { _decorator, Component, Node, Color } from 'cc';
import { ASSET_KEY } from '../lib/enum/asset';
import { BaseSprite } from '../lib/sprite/baseSprite';
const { ccclass, property } = _decorator;

@ccclass('GameBackgroundSprite')
export class GameBackgroundSprite extends BaseSprite {
    private readonly bgColor = new Color(128, 58, 42);
    
    constructor() {
        super('GameBackgroundSprite', ASSET_KEY.WHITE_BOX_SPRITE);
    }

    onLoad() {
        super.onLoad();
        this.setColor(this.bgColor);
    }
}