import { _decorator, Component, Node } from 'cc';
import { ASSET_KEY } from '../lib/enum/asset';
import { BaseSprite } from '../lib/sprite/baseSprite';
const { ccclass, property } = _decorator;

@ccclass('KeypadLeftSprite')
export class KeypadLeftSprite extends BaseSprite {
    constructor() {
        super('KeypadLeftSprite', ASSET_KEY.KEYPAD_SPRITESHEET, 3);
    }
}