import { _decorator, Component, Node } from 'cc';
import { ASSET_KEY } from '../lib/enum/asset';
import { BaseAudio } from './baseAudio';
const { ccclass, property } = _decorator;

@ccclass('BackgroundSoundtrack')
export class BackgroundSoundtrack extends BaseAudio {
    constructor() {
        super('BackgroundSoundtrack', ASSET_KEY.BACKGROUND_SOUNDTRACK, true, 0.5);
    }
}