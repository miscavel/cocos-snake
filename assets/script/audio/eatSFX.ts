import { _decorator, Component, Node } from 'cc';
import { ASSET_KEY } from '../lib/enum/asset';
import { BaseAudio } from './baseAudio';
const { ccclass, property } = _decorator;

@ccclass('EatSFX')
export class EatSFX extends BaseAudio {
    constructor() {
        super('EatSFX', ASSET_KEY.EAT_SFX, false, 0.4);
    }
}