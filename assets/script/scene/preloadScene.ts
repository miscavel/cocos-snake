import { _decorator, Component, director } from 'cc';
import { SilenceSFX } from '../audio/silenceSFX';
import { PreloadControl } from '../control/preloadControl';
import { PRELOAD_CONTROL_EVENT } from '../enum/preloadControl';
import { SCENE_KEY } from '../enum/scene';
import { ASSET_LOADER_EVENT } from '../lib/enum/assetLoader';
import { AssetLoader } from '../lib/loader/assetLoader';
import { AssetLoadingUI } from '../lib/loader/assetLoadingUI';
const { ccclass, property } = _decorator;

@ccclass('PreloadScene')
export class PreloadScene extends Component {
    @property(AssetLoader)
    public readonly assetLoader?: AssetLoader;

    @property(AssetLoadingUI)
    public readonly assetLoadingUI?: AssetLoadingUI;

    @property(PreloadControl)
    public readonly preloadControl?: PreloadControl;

    @property(SilenceSFX)
    public readonly silenceSFX?: SilenceSFX;

    start() {
        this.startAssetsLoad();
    }

    private startAssetsLoad() {
        const { assetLoader, assetLoadingUI } = this;

        if (!assetLoader || !assetLoadingUI) return;

        assetLoader.node.on(ASSET_LOADER_EVENT.START, (progress: number) => {
            assetLoadingUI.updateText(progress);
        });

        assetLoader.node.on(ASSET_LOADER_EVENT.ASSET_LOAD_SUCCESS, (progress: number, key:string) => {
            assetLoadingUI.updateText(progress, key);
        });

        assetLoader.node.on(ASSET_LOADER_EVENT.COMPLETE, (progress: number) => {
            assetLoadingUI.updateText(progress);
            this.onComplete();
        });

        assetLoader.startAssetsLoad();
    }

    private onComplete() {
        this.silenceSFX?.play(); // This is here to bypass audio autoplay on web

        this.preloadControl?.registerTouchEvent();
        this.preloadControl?.node.once(PRELOAD_CONTROL_EVENT.TOUCH_END, () => {
            this.goToTitleScene();
        });
    }

    private goToTitleScene() {
        director.loadScene(SCENE_KEY.TITLE);
    }
}