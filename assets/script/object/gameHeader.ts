
import { _decorator, Component, Node } from 'cc';
import { GuideHeader } from './guideHeader';
import { ScoreHeader } from './scoreHeader';
const { ccclass, property } = _decorator;

@ccclass('GameHeader')
export class GameHeader extends Component {
    @property(GuideHeader)
    public readonly guideHeader?: GuideHeader;

    @property(ScoreHeader)
    public readonly scoreHeader?: ScoreHeader;

    start() {
        this.hideScore();
        this.showGuide();
    }

    public hideGuide() {
        this.guideHeader?.hide();
    }

    public showGuide() {
        this.guideHeader?.show();
    }

    public hideScore() {
        this.scoreHeader?.hide();
    }

    public showScore() {
        this.scoreHeader?.show();
    }

    public updateScore(score: number) {
        this.scoreHeader?.updateScore(score);
    }

    public updateHighscore(highscore: number) {
        this.scoreHeader?.updateHighscore(highscore);
    }
}
