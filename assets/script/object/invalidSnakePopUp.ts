import { _decorator, Component, Node, Color } from 'cc';
import { BUTTON_EVENT } from '../enum/button';
import { INVALID_SNAKE_POP_UP_EVENT } from '../enum/invalidSnakePopUp';
import { BasePopUp } from '../lib/object/basePopUp';
import { TransitionScreen } from '../sprite/transitionScreen';
import { BaseButton } from './baseButton';
const { ccclass, property } = _decorator;

@ccclass('InvalidSnakePopUp')
export class InvalidSnakePopUp extends BasePopUp {
    @property(BaseButton)
    public readonly cancelButton?: BaseButton;

    @property(TransitionScreen)
    public readonly darkScreen?: TransitionScreen;
    
    public show() {
      super.show();
      this.darkScreen?.fadeIn(0.3, Color.BLACK, 122); 
    }

    public registerTouchEvent() {
      const { cancelButton } = this;

      cancelButton?.node.on(BUTTON_EVENT.TOUCH_END, () => {
        this.node.emit(INVALID_SNAKE_POP_UP_EVENT.CANCEL);
      });

    }

    public unregisterTouchEvent() {
      const { cancelButton } = this;

      cancelButton?.node.off(BUTTON_EVENT.TOUCH_END);
    }
}