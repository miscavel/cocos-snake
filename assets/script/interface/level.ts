import { BoardConfig } from "./board";
import { SnakeConfig } from "./snake";

export interface LevelConfig {
  boardConfig: BoardConfig;
  snakeConfig: SnakeConfig;
}